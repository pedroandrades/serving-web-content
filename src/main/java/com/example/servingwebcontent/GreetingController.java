package com.example.servingwebcontent;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

	@GetMapping("/sum")
	public String sum(@RequestParam(defaultValue = "0") Integer a,
			@RequestParam(defaultValue = "0") Integer b, Model model) {
		model.addAttribute("sum", a + b);

		return "sum";
	}
	
	@GetMapping("/sumForm")
	public String sumForm(Model model) {
		return "sumForm";
	}
	
	@GetMapping("/greeting")
	public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name,
			Model model) {
		model.addAttribute("name", name);
		return "greeting";
	}

}